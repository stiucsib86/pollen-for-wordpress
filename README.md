# README #

This is the development branch for "Pollen Publisher Tools" Wordpress Plugin http://plugins.svn.wordpress.org/pollen-publisher-tools/

### What is this repository for? ###

* Wordpress plugin for https://getpollen.co
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Checkout this repository.
* Only edit content in "trunk" folder.
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Deployment instructions? ###
* It should be clear under https://wordpress.org/plugins/about/svn/. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Developers: team@getpollen.co
